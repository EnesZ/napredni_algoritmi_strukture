#include <iostream>
#include <vector>
#include "heap.h"

using namespace std;

int main()
{

    Heap* h = new Heap();
    h->add(2);
    h->add(1);
    h->add(5);
    h->add(4);

    h->print();
    cout<<endl;
    cout<<endl<<"Extract minimum"<<endl;
    h->extract_min();
    h->print();
    cout<<endl;
    cout<<endl<<"Decrease node 5 to node 0"<<endl;
    decrease_key(h->heap.at(1)->child, 0);
    h->print();
    cout<<endl;
    cout<<endl<<"Delete node 4"<<endl;
    h->delete_node(h->heap.at(1)->child);
    h->print();
    return 0;
}
