
#ifndef NODE_H
#define NODE_H

#include <vector>
#include <algorithm>
using namespace std;

struct Node
{
    int value,degree;
    Node *child,*parent, *sibling;
    Node():value(0),degree(0),child(NULL), parent(NULL), sibling(NULL){};
    Node(int x):value(x),degree(0),child(NULL), parent(NULL), sibling(NULL){};
    void print_node(Node* n);
};
void Node::print_node(Node* n){
    if(n==NULL)
        return;

    cout<<"node: "<<n->value<<" degree"<<n->degree<<" ";
    print_node(n->sibling);
    print_node(n->child);
}

void decrease_key(Node* n,int new_key){
    if(n==NULL || n->value<=new_key)
        return;

    while(n->parent!=NULL && n->parent->value > new_key){
        n->value = n->parent->value;
        n->parent->value = new_key;
        n = n->parent;
    }

}

#endif // NODE_H
