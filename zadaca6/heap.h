#ifndef HEAP_H
#define HEAP_H

#include "node.h"
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

struct Heap
{
    vector<Node* > heap;
    Heap(){};
    Heap(Node* n){heap.push_back(n);};
    void add(int x);
    void print();
    vector<Node* > union_heap(vector<Node* > v1, vector<Node* > v2);
    void extract_min();
    vector<Node* > delete_node(Node* n);
};

vector<Node* > Heap::delete_node(Node* n){
    decrease_key(n,INT_MIN);
    extract_min();
}

void Heap::extract_min(){
    int m = INT_MAX;
    Node* min_n = NULL;
    int idx = -1;
    for(int i=0;i<heap.size();i++){
        if(heap.at(i)->value < m){
            m=heap.at(i)->value;
            idx = i;
            min_n = heap.at(i);
        }
    }
    vector<Node* > new_vec;
    Node* temp = heap.at(idx)->child;
    Node* temp1 = temp;
    while(temp){
        temp->parent=NULL;
        new_vec.push_back(temp);
        temp = temp->sibling;
        temp1->sibling = NULL;
        temp1 = temp;
    }

    heap.erase(heap.begin()+idx);
    reverse(new_vec.begin(),new_vec.end());

    heap = union_heap(new_vec,heap);


}

void Heap::print(){
    for(int i=0;i<heap.size();i++){
        cout<<endl<<"Heap root: "<<i<<endl;
        heap.at(i)->print_node(heap.at(i));
    }

}
void Heap::add(int x){
    Node* n = new Node(x);
    vector<Node* > v,k;
    v.push_back(n);
    k = heap;
    heap = union_heap(k,v);
}


vector<Node* > Heap::union_heap(vector<Node* > v1, vector<Node* > v2){
    vector<Node* > v;
    int i=0;
    int j=0;

    while(i<v1.size()&&j<v2.size()){
        if(v1.at(i)->degree < v2.at(j)->degree){
            v.push_back(v1.at(i));
            i++;
        }else{
            v.push_back(v2.at(j));
            j++;
        }
    }
    while(i<v1.size()){
        v.push_back(v1.at(i));
        i++;
    }
    while(j<v2.size()){
        v.push_back(v2.at(j));
        j++;
    }

    i=0;
    while(i<v.size()-1){
        if(v.at(i)->degree == v.at(i+1)->degree){
            if(i+2 < v.size() && v.at(i+1)->degree == v.at(i+2)->degree){
                i++;
                continue;
            }else{
                if(v.at(i)->value < v.at(i+1)->value){
                    v.at(i+1)->parent = v.at(i);

                    v.at(i+1)->sibling = v.at(i)->child;
                    v.at(i)->child = v.at(i+1);
                    v.at(i)->degree++;
                    v.erase(v.begin()+i+1);
                }else{
                    v.at(i)->parent = v.at(i+1);

                    v.at(i)->sibling = v.at(i+1)->child;
                    v.at(i+1)->child = v.at(i);
                    v.at(i+1)->degree++;
                    v.erase(v.begin()+i);
                }
            }
        }else{
            i++;
        }
    }
    return v;

}







#endif // HEAP_H
