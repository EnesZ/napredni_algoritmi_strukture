#include <iostream>
#include <vector>

using namespace std;

template <class T>
void ispisi_vec(vector<T>& v){
    for(int i=0;i< v.size(); i++){
        cout<<v.at(i)<<" ";
    }
    cout<<endl;
}

void permutation_SAT(int n, int depth, vector<bool> v){
    if(depth == n){
        ispisi_vec(v);
        return;
    }
    vector<bool> v1 = v;
    v.push_back(true);
    v1.push_back(false);
    permutation_SAT(n,depth+1,v);
    permutation_SAT(n,depth+1,v1);
    //return permutation_SAT(n,depth+1,v.push_back(false));
}

void permutation(int depth,int n, vector<int> v1, vector<int> v2){
    if(depth == n){
        ispisi_vec(v2);
        return;
    }
    int x = v1.back();
    v1.pop_back();
    for(int i=0;i<v2.size();i++){
        //cout<<i<<' ';
        vector<int> temp_v = v2;
        if (temp_v.at(i)==-1){
            temp_v.at(i)=x;
            permutation(depth+1,n,v1,temp_v);
        }
    }
}
void subset2(int n,int depth ,vector<vector<int> >& p){
    if(depth>n){
        return;
    }

    if(depth==0){
        vector<int> x;
        p.push_back(x);
        subset2(n,depth+1,p);
    }else{
        int size_p = p.size();
        for(int i=0;i<size_p ;i++){
            vector<int> temp = p.at(i);
            temp.push_back(depth);
            p.push_back(temp);
        }
        subset2(n,depth+1,p);
    }
}

vector<vector<int> > combination(int n, int k){
    vector<vector<int> > p;
    vector<vector<int> > comb;
    subset2(n,0,p);
    for(int i=0;i<p.size();i++){
        if(p.at(i).size()==k){
            //ispisi_vec(p.at(i));
            comb.push_back(p.at(i));
        }
    }
    return comb;
}
void variation(int n, int k){
    vector<vector<int> > comb =combination(n,k);
    vector<int> temp;
    for (int i=0;i<k;i++)
        temp.push_back(-1);
    for(int i=0;i<comb.size();i++){
        permutation(0,k,comb.at(i),temp);
    }
}


int main()
{
    //test sat
    /*vector<bool> x;
    permutation_SAT(4,0,x);*/

    //test permutation
    /*vector<int> v1,v2;
    int n = 4;
    for(int i=0;i<n;i++){
        v1.push_back(i);
        v2.push_back(-1);
    }
    permutation(0,n,v1,v2);*/

    //test subset
    /*vector<vector<int> > p;
    subset2(4,0,p);
    for(int i=0;i<p.size();i++){
        ispisi_vec(p.at(i));

    }*/

    //test combinations
    /*vector<vector<int> > comb=combination(3,2);
    for(int i=0;i<comb.size();i++){
        ispisi_vec(comb.at(i));
    }*/

    //test variations
    /*variation(3,2);*/

    return 0;
}
