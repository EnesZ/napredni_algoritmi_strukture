#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>

#include "node.h"

using namespace std;

double distance(double x1, double y1, double x2, double y2){
    return sqrt(pow(x1-x2,2)+pow(y1-y2,2));
}

vector<vector<double> > make_adj_matrix(vector<int>& indices, vector<double>& lat, vector<double>& lon){

    vector<vector<double> > adj_matrix;
    vector<double> temp;
    //initialize adj_matrix
    for(int i=0;i<indices.size();i++)
        temp.push_back(0);
    for(int i=0;i<indices.size();i++)
        adj_matrix.push_back(temp);

    //calculate adjacency matrix
    for(int i=0;i<indices.size();i++){
        for(int j=i+1;j<indices.size();j++){
            double d = distance(lat.at(i),lon.at(i),lat.at(j),lon.at(j));
            adj_matrix.at(i).at(j)=d;
            adj_matrix.at(j).at(i)=d;
        }
    }
    return adj_matrix;
}

bool is_cycle(Node* x, Node* y, bool traverse = false){
    Node* temp = x;
    Node* real_prev = x;

    if(temp->next != NULL){
        temp = temp->next;
    }else{
        temp = temp->prev;
    }

    if(traverse)
        cout<<real_prev->index<<"--";
    while(temp){

        if(traverse)
            cout<<temp->index<<"--";

        //cycle
        if(temp->index == y->index)
            return true;

        //check in which side to go next
        //check from which side we came
        if(temp->next && temp->next->index == real_prev->index){
            real_prev = temp;
            temp=temp->prev;
        }else{
            real_prev = temp;
            temp = temp->next;
        }

    }

    return false;

}


int main()
{
    //read file

    ifstream infile("bih.txt");
    int a;
    double b,c;
    vector<int> indices;
    vector<double> latitude;
    vector<double> longitude;
    while (infile >> a >> b >> c)
    {
        indices.push_back(a);
        latitude.push_back(b);
        longitude.push_back(c);
    }

    //Make adjacency matrix
    vector<vector<double> > adj_matrix = make_adj_matrix(indices,latitude,longitude);

    //Create nodes
    vector<Node*> nodes;
    for(int i=1;i<indices.size();i++){
        Node* n = new Node(i,adj_matrix.at(0).at(i));
        nodes.push_back(n);
    }

    //Calculate cost at the beginning
    double s=0;
    for(int i=0;i<nodes.size();i++)
        s+= nodes.at(i)->d20;
    cout<<"Total cost: "<<2*s<<endl;


    s = 0;
    while(nodes.size()>2){


        double max_saving = 0;
        int node1_idx = -1;
        int node2_idx = -1;
        for(int i=0;i<nodes.size();i++){

            //Get distance from node i to 0
            double d1 = nodes.at(i)->d20;

            for(int j=i+1; j<nodes.size();j++){

                // if node(j) is connected at one side
                if(nodes.at(j)->next!= NULL || nodes.at(j)->prev!= NULL){
                    // if node(i) is connected at one side
                    if(nodes.at(i)->next!=NULL || nodes.at(i)->prev!=NULL){

                        //check for cycle
                        if(is_cycle(nodes.at(j),nodes.at(i)))
                            continue;
                    }
                }
                //get distance from node j to 0
                double d2 = nodes.at(j)->d20;
                //calculate saving
                double saving = d1 + d2 - adj_matrix.at(nodes.at(i)->index).at(nodes.at(j)->index);
                //check for max saving
                if(saving > max_saving){
                    max_saving = saving;
                    node1_idx = i;
                    node2_idx = j;
                }
            }
        }

        //add distance from chosen nodes
        s+=adj_matrix.at(nodes.at(node1_idx)->index).at(nodes.at(node2_idx)->index);

        //connect nodes
        if(nodes.at(node1_idx)->next==NULL)
            nodes.at(node1_idx)->next=nodes.at(node2_idx);
        else
            nodes.at(node1_idx)->prev=nodes.at(node2_idx);

        if(nodes.at(node2_idx)->next==NULL)
            nodes.at(node2_idx)->next = nodes.at(node1_idx);
        else
            nodes.at(node2_idx)->prev = nodes.at(node1_idx);

        // if node is connected at both sides erase node from nodes vector
        if(nodes.at(node1_idx)->next!=NULL && nodes.at(node1_idx)->prev!=NULL){
            nodes.erase(nodes.begin()+node1_idx);
            //if erased node is in front of the 2nd node in the nodes vector
            if(node1_idx<node2_idx)
                node2_idx--;
        }
        if(nodes.at(node2_idx)->next!=NULL && nodes.at(node2_idx)->prev!=NULL)
            nodes.erase(nodes.begin()+node2_idx);

    }

    // add distance from node 0 to remaining nodes in vector nodes
    s+=nodes.at(0)->d20;
    s+=nodes.at(1)->d20;
    cout << "Final route cost: "<<s<<endl;

    //write route
    cout<<"0--";
    is_cycle(nodes.at(0),nodes.at(1),true);
    cout<<"0--";

    return 0;
}
